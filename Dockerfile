ARG VERSION=latest

FROM alpine:${VERSION}

COPY install.sh /root/

RUN /bin/sh /root/install.sh

COPY check-docker-dependency-and-trig-ci.sh /root/

ENTRYPOINT /root/check-docker-dependency-and-trig-ci.sh