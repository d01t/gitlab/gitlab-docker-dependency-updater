#!/bin/bash
set -e
VARIABLE_NAMES=""
for SOURCE_FILE in .env secrets/.env
do
	if [ -f "${SOURCE_FILE}" ]
	then
		if [ -n "${VARIABLE_NAMES}" ]
		then
			VARIABLE_NAMES="${VARIABLE_NAMES}|"
		fi 
		VARIABLE_NAMES="${VARIABLE_NAMES}$(grep = "${SOURCE_FILE}" | grep -v '#' | cut -d= -f1 | xargs | sed -e 's/ /|/g')"
	fi
done
if env | grep -q -E "^(${VARIABLE_NAMES})="
then
	VARIABLES=$(env| grep -E "^(${VARIABLE_NAMES})=")
fi
for SOURCE_FILE in .env secrets/.env
do
	if [ -f "${SOURCE_FILE}" ]
	then
		source "${SOURCE_FILE}"
		export $(grep = "${SOURCE_FILE}" | grep -v '#' | cut -d= -f1)
	fi
done
if [ -n "${VARIABLES}" ]
then
	export ${VARIABLES}
fi

urlencode() {
	# urlencode <string>
	old_lc_collate=$LC_COLLATE
	LC_COLLATE=C
	
	local length="${#1}"
	for (( i = 0; i < length; i++ )); do
		local c="${1:i:1}"
		case $c in
			[a-zA-Z0-9.~_-]) printf "$c" ;;
			*) printf '%%%02X' "'$c" ;;
		esac
	done
	
	LC_COLLATE=$old_lc_collate
}

urldecode() {
	# urldecode <string>

	local url_encoded="${1//+/ }"
	printf '%b' "${url_encoded//%/\\x}"
}

if [ -z "${GITLAB_URL}" ] || [ -z "${GITLAB_TOKEN}" ]
then
	echo "Missing environment variables \${GITLAB_URL} or \${GITLAB_TOKEN}"
	echo "You can either define them in a local .env file or directly from shell prompt"
fi

CURL="curl --silent --header"
GITLAB_API="https://${GITLAB_URL}/api/v4"
${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" \
	"${GITLAB_API}/projects?membership=true&min_access_level=30" | \
	jq '.[].id' | \
	while read PROJECT_ID
do
	${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" \
		"${GITLAB_API}/projects/${PROJECT_ID}/search?scope=blobs&search=FROM%20filename:Dockerfile" | \
		jq --compact-output '.[]|select(.filename|contains("Dockerfile")) | { filename, ref }' | \
		while read SEARCH_RESULT
	do
		FILENAME=$(echo ${SEARCH_RESULT} | jq '.filename' | sed -e 's/"//g')
		REF=$(echo ${SEARCH_RESULT} | jq '.ref' | sed -e 's/"//g')
		ENCODED_FILENAME=$(urlencode ${FILENAME})
		LAST_PIPELINES=$(${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" "${GITLAB_API}/projects/${PROJECT_ID}/pipelines?ref=${REF}&status=success")
		if echo "${LAST_PIPELINES}" | grep -q "403 Forbidden"
		then
			continue
		fi
		PIPELINE_ID=$(echo "${LAST_PIPELINES}" | jq '.[0].id' || true)
		if [ -z "${PIPELINE_ID}" -o "${PIPELINE_ID}" == "null" ]
		then
			continue
		fi
		PIPELINE_CREATED_AT=$(date -d "$(${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" "${GITLAB_API}/projects/${PROJECT_ID}/pipelines/${PIPELINE_ID}" | jq '.created_at' | sed -e 's/"//g' -e 's/T/ /g' -e 's/\..*//g')" +%s)
		${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" \
			"${GITLAB_API}/projects/${PROJECT_ID}/repository/files/${ENCODED_FILENAME}/raw?ref=${REF}" | \
			grep -E '^FROM [^:]+:?' | \
			sed -E 's/^FROM ([^ ]+).*/\1/g' | while read DOCKER_DEPENDENCY
		do
			DOCKER_DEPENDENCY_NAME=$(echo ${DOCKER_DEPENDENCY} | sed -e 's/:.*//g')
			DOCKER_DEPENDENCY_TAG=$(echo ${DOCKER_DEPENDENCY} | sed -E 's/^[^:]+://g')
			if echo ${DOCKER_DEPENDENCY_TAG} | grep -v -q -i -E '^[\._a-z0-9-]+$'
			then
				DOCKER_DEPENDENCY_TAG=latest
			fi
			DOCKER_REGISTRY_DOMAIN="registry.hub.docker.com"
			if echo ${DOCKER_DEPENDENCY_NAME} | grep -q '/'
			then
				if echo ${DOCKER_DEPENDENCY_NAME} | sed -e 's#/.*##g' | grep -q '\.'
				then
					DOCKER_REGISTRY_DOMAIN=$(echo ${DOCKER_DEPENDENCY_NAME} | sed -e 's#/.*##g')
					DOCKER_REGISTRY_PATH=$(echo ${DOCKER_DEPENDENCY_NAME} | sed -E 's#^[^/]+/##g')
					DOCKER_REGISTRY_URL="${DOCKER_REGISTRY_PATH}/manifests/${DOCKER_DEPENDENCY_TAG}"
				else
					DOCKER_REGISTRY_URL="repositories/${DOCKER_DEPENDENCY_NAME}/"
				fi
			else
				DOCKER_REGISTRY_URL="repositories/library/${DOCKER_DEPENDENCY_NAME}/"
			fi
			DOCKER_REGISTRY_URL="https://${DOCKER_REGISTRY_DOMAIN}/v2/${DOCKER_REGISTRY_URL}"
			if [ "${DOCKER_REGISTRY_DOMAIN}" == "registry.gitlab.com" ] && [ "${GITLAB_URL}" == "gitlab.com" ] 
			then
				BEARER_TOKEN=$(${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" "https://${GITLAB_URL}/jwt/auth?client_id=docker&offline_token=true&service=container_registry&scope=repository:${DOCKER_REGISTRY_PATH}:push,pull" | jq -r .token)
				DOCKER_REGISTRY_RESPONSE=$(${CURL} "Authorization: Bearer ${BEARER_TOKEN}" ${DOCKER_REGISTRY_URL} | jq -r '.history[].v1Compatibility' | jq '.created' | sort | tail -n1)
			else
				DOCKER_REGISTRY_RESPONSE=$(curl --silent ${DOCKER_REGISTRY_URL} | jq -r '.last_updated')
			fi
			DOCKER_LAST_UPDATED=$(date -d "$(echo "${DOCKER_REGISTRY_RESPONSE}"   | sed -e 's/"//g' -e 's/T/ /g' -e 's/\..*//g')" +%s)
			if [ "${DOCKER_LAST_UPDATED}" -gt "${PIPELINE_CREATED_AT}" ]
			then
				${CURL} "Authorization: Bearer ${GITLAB_TOKEN}" --request POST "${GITLAB_API}/projects/${PROJECT_ID}/pipeline?ref=${REF}" -o /dev/null
				break 2
			fi
		done 
	done
done