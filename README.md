# GitLab Docker dependency updater

A shell script that browses each repository that your account can access, searches for Docker file, checks if Docker base image
is newer than the latest pipeline on considered branch, then triggers a new pipeline if needed


## Requirements

- bash
- jq
- sed (Gnu version)
- grep

*or*

- Docker


## Prerequisites

You have to create a Personal Access Token for account with "api" and "read_registry" permissions.

See https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html

## Usage

Bare metal
```bash
$ git clone git@gitlab.com:d01t/gitlab-docker-dependency-updater.git
$ cd gitlab-docker-dependency-updater
$ GITLAB_URL=gitlab.com GITLAB_TOKEN=xxxxxxxxxxx ./check-docker-dependency-and-trig-ci.sh
```

Docker
```bash
$ docker run -e "GITLAB_URL=gitlab.com" -e "GITLAB_TOKEN=xxxxxxxxxxx" registry.gitlab.com/d01t/gitlab-docker-dependency-updater:latest ./check-docker-dependency-and-trig-ci.sh
```

## Remarks

You can schedule this script through cron, but don't call this script each minute, because it tests if pipeline has succeed or not :
by calling several times, you'll create duplicate pipeline.

This script doesn't resolve each parent image in registry neither an order to call, so you'll have to be patient between each call : a good
practice can be a call every hour max.